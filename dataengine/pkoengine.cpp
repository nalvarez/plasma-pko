/*
 *   Copyright (C) 2011 by Nicolas Alvarez <nicolas.alvarez@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 2 of
 *  the License or (at your option) version 3 or any later version
 *  accepted by the membership of KDE e.V. (or its successor approved
 *  by the membership of KDE e.V.), which shall act as a proxy
 *  defined in Section 14 of version 3 of the license.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "pkoengine.h"
#include "pkoloader.h"

PkoEngine::PkoEngine(QObject *parent, const QVariantList &args)
    : Plasma::DataEngine(parent, args)
{
}

PkoEngine::~PkoEngine()
{
}
void PkoEngine::init()
{
    PkoLoaderJob* job = loadPko();
    Q_ASSERT(job);
    connect(job, SIGNAL(result(KJob*)), SLOT(dataFetched(KJob*)));
    job->start();
}
void PkoEngine::dataFetched(KJob* job)
{
    PkoLoaderJob* pkoJob = qobject_cast<PkoLoaderJob*>(job);
    Q_ASSERT(pkoJob);
    foreach(const PkoItem& sitem, pkoJob->getData()) {
        if (!sitem.hasRepo()) continue;
        const QString& id = sitem.identifier();
        setData(id, "name", sitem.name());
        setData(id, "pko-web", sitem.web());
        setData(id, "pko-repo", sitem.pkoRepoViewer());
        setData(id, "description", sitem.description());
    }
}

K_EXPORT_PLASMA_DATAENGINE(pko, PkoEngine)

#include "pkoengine.moc"
