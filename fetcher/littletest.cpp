/*
 *   Copyright (C) 2011 by Nicolas Alvarez <nicolas.alvarez@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 2 of
 *  the License or (at your option) version 3 or any later version
 *  accepted by the membership of KDE e.V. (or its successor approved
 *  by the membership of KDE e.V.), which shall act as a proxy
 *  defined in Section 14 of version 3 of the license.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "pkoloader.h"

#include <kdebug.h>
#include <kapplication.h>
#include <kcmdlineargs.h>

struct MainTest: public QObject {
Q_OBJECT;
public slots:
    void done(KJob* job) {
        kDebug() << "success:" << job->error();
        PkoLoaderJob* pkoJob = qobject_cast<PkoLoaderJob*>(job);
        Q_ASSERT(pkoJob);
        kDebug() << pkoJob->getData().size();
        foreach(const PkoItem& sitem, pkoJob->getData()) {
            printIt(sitem, 1);
        }
        QMetaObject::invokeMethod(kapp, "quit", Qt::QueuedConnection);
    }
public:
    void printIt(const PkoItem& item, int indent) {
        kDebug() << qPrintable(QString(indent, ' ')) << item.identifier() << item.name();
    }
    void start() {
        PkoLoaderJob* job = loadPko();
        job->start();
        connect(job, SIGNAL(result(KJob*)), SLOT(done(KJob*)));
    }
};


int main(int argc, char** argv) {
    KCmdLineArgs::init(argc, argv, "littletest","",ki18n(""), "");
    KApplication app(false);

    MainTest t;
    t.start();
    return app.exec();
}
#include "littletest.moc"

