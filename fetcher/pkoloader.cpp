/*
 *   Copyright (C) 2011 by Nicolas Alvarez <nicolas.alvarez@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 2 of
 *  the License or (at your option) version 3 or any later version
 *  accepted by the membership of KDE e.V. (or its successor approved
 *  by the membership of KDE e.V.), which shall act as a proxy
 *  defined in Section 14 of version 3 of the license.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "pkoloader.h"

#include <kio/job.h>
#include <QtXml/QDomDocument>

bool parse(const QDomDocument& doc, QList<PkoItem>& result);
bool parseItem(const QDomElement& elem, QList<PkoItem>& result);

PkoLoaderJob* loadPko()
{
    return new PkoLoaderJob;
}

PkoLoaderJob::PkoLoaderJob()
{
}

void PkoLoaderJob::start()
{
    this->m_job = KIO::storedGet(KUrl("http://projects.kde.org/kde_projects.xml"), KIO::NoReload, KIO::HideProgressInfo);

    connect(m_job, SIGNAL(result(KJob*)), SLOT(gotFinished(KJob*)));
}
void PkoLoaderJob::gotFinished(KJob* job)
{
    Q_ASSERT(job == m_job);
    // TODO check if HTTP code was 200

    QDomDocument doc;
    doc.setContent(m_job->data()); //TODO check return code
    parse(doc, m_pkoitems); //TODO check return code
    emitResult();

    m_job = 0;
}
QList<PkoItem> PkoLoaderJob::getData() const
{
    return m_pkoitems;
}


bool parse(const QDomDocument& doc, QList<PkoItem>& result)
{
    // TODO all these asserts should be proper error checks instead
    QList<PkoItem> result_l;

    QDomElement root = doc.documentElement();
    Q_ASSERT(root.tagName() == "kdeprojects");
    for (QDomElement elem = root.firstChildElement(); !elem.isNull(); elem = elem.nextSiblingElement()) {
        Q_ASSERT(elem.tagName() == "component");
        Q_ASSERT(parseItem(elem, result_l));
    }

    qSwap(result, result_l);
    return true;
}

bool parseItem(const QDomElement& elem, QList<PkoItem>& result)
{
    PkoItem item;
    {
    QDomElement nameNode = elem.lastChildElement("name");
    if (nameNode.isNull()) return false;
    item.m_name = nameNode.text();
    }
    {
        QString idAttr = elem.attribute("identifier");
        if (idAttr.isEmpty()) return false;
        item.m_identifier = idAttr;
    }
    {
        QDomElement webNode = elem.lastChildElement("web");
        if (webNode.isNull()) return false;
        item.m_web = QUrl(webNode.text());
    }
    {
        QDomElement descNode = elem.lastChildElement("description");
        if (descNode.isNull()) return false;
        item.m_description = descNode.text();
    }
    {
        QDomElement repoNode = elem.lastChildElement("repo");
        item.m_hasRepo = !repoNode.isNull();
        if (!repoNode.isNull()) {
            for(QDomElement subelem = repoNode.firstChildElement("web"); !subelem.isNull(); subelem = subelem.nextSiblingElement("web")) {
                if(subelem.attribute("type") == "projects") {
                    item.m_pkoRepoViewer = QUrl(subelem.text());
                }
            }
        }
    }
    result.append(item);

    QString childTagName;
    if (elem.tagName() == "component") {
        childTagName = "module";
    } else if (elem.tagName() == "module" || elem.tagName() == "project") {
        childTagName = "project";
    } else {
        return false;
    }
    for (QDomElement subelem = elem.firstChildElement(childTagName); !subelem.isNull(); subelem = subelem.nextSiblingElement(childTagName)) {
        Q_ASSERT(parseItem(subelem, result));
    }

    return true;
}



#include "pkoloader.moc"
