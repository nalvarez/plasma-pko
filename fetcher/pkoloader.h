/*
 *   Copyright (C) 2011 by Nicolas Alvarez <nicolas.alvarez@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 2 of
 *  the License or (at your option) version 3 or any later version
 *  accepted by the membership of KDE e.V. (or its successor approved
 *  by the membership of KDE e.V.), which shall act as a proxy
 *  defined in Section 14 of version 3 of the license.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PKOLOADER_H
#define PKOLOADER_H

#include <QObject>
#include <QString>
#include <QList>
#include <QUrl>
#include <kjob.h>

class QDomDocument;
class QDomElement;
namespace KIO {
class StoredTransferJob;
}

class PkoItem {
public:
    PkoItem(): m_hasRepo(false) {}

    QString identifier() const { return m_identifier; }
    QString name() const { return m_name; }
    QUrl web() const { return m_web; }
    QString description() const { return m_description; }
    bool hasRepo() const { return m_hasRepo; }
    QUrl pkoRepoViewer() const { return m_pkoRepoViewer; }

private:
    QString m_identifier;
    QString m_name;
    QUrl m_web;
    QString m_description;

    bool m_hasRepo;
    QUrl m_pkoRepoViewer;

    friend bool parseItem(const QDomElement& elem, QList<PkoItem>& result);
};

class PkoLoaderJob: public KJob {
    Q_OBJECT
public:
    PkoLoaderJob();
    void start();
    QList<PkoItem> getData() const;

private slots:
    void gotFinished(KJob* job);

private:
    KIO::StoredTransferJob* m_job;
    QList<PkoItem> m_pkoitems;
};

PkoLoaderJob* loadPko();

#endif
