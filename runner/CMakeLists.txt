kde4_add_plugin(pkorunner pkorunner.cpp ../fetcher/pkoloader.cpp)
target_link_libraries(pkorunner ${KDE4_PLASMA_LIBS} ${KDE4_KIO_LIBS} ${QT_QTXML_LIBRARY})

install(TARGETS pkorunner DESTINATION ${PLUGIN_INSTALL_DIR})

install(FILES plasma-runner-pko.desktop DESTINATION ${SERVICES_INSTALL_DIR})
