/*
 *   Copyright (C) 2011 by Nicolas Alvarez <nicolas.alvarez@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 2 of
 *  the License or (at your option) version 3 or any later version
 *  accepted by the membership of KDE e.V. (or its successor approved
 *  by the membership of KDE e.V.), which shall act as a proxy
 *  defined in Section 14 of version 3 of the license.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "pkorunner.h"

#include <KIcon>
#include <KToolInvocation>

PkoRunner::PkoRunner(QObject *parent, const QVariantList &args)
    : Plasma::AbstractRunner(parent, args)
{
}

PkoRunner::~PkoRunner()
{
}
void PkoRunner::init()
{
    PkoLoaderJob* job = loadPko();
    Q_ASSERT(job);
    connect(job, SIGNAL(result(KJob*)), SLOT(dataFetched(KJob*)));
    job->start();
}
void PkoRunner::dataFetched(KJob* job)
{
    PkoLoaderJob* pkoJob = qobject_cast<PkoLoaderJob*>(job);
    Q_ASSERT(pkoJob);
    pkoData = pkoJob->getData();
}

enum Kind { KIND_PKO, KIND_REPO };

void PkoRunner::match(Plasma::RunnerContext& context)
{
    QString runnerQuery = context.query();

    Kind k;

    if (runnerQuery.startsWith("pko ")) {
        runnerQuery.remove(0, 4); // remove leading "pko "
        k = KIND_PKO;
    } else if (runnerQuery.startsWith("repo ")) {
        runnerQuery.remove(0, 5); // remove leading "repo "
        k = KIND_REPO;
    } else {
        return;
    }

    if (runnerQuery.length() < 2) return;

    const KIcon kdeIcon("kde");
    foreach(const PkoItem& project, pkoData) {
        if (project.identifier().contains(runnerQuery)) {

            Plasma::QueryMatch match(this);
            match.setType(Plasma::QueryMatch::PossibleMatch);
            match.setText(i18n("%1 (projects.kde.org)", project.identifier()));
            match.setSubtext(project.name());
            match.setIcon(kdeIcon);
            if (k == KIND_PKO) {
                match.setData(project.web());
            } else if (k == KIND_REPO) {
                match.setData(project.pkoRepoViewer());
            }
            context.addMatch(context.query(), match);
        }
    }
}
void PkoRunner::run(const Plasma::RunnerContext& context, const Plasma::QueryMatch& match)
{
    Q_UNUSED(context);
    KToolInvocation::invokeBrowser(match.data().toUrl().toString());
}


K_EXPORT_PLASMA_RUNNER(pko, PkoRunner)

#include "pkorunner.moc"
